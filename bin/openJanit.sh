#!/bin/bash

window=`wmctrl -l | grep Janit_MENU`
#windowID=`echo $window | cut -d " " -f1`

if [ -z "$window" ]
then
  #open new window
  mate-terminal -t Janit_MENU -e /usr/bin/janit
  sleep .3
  wmctrl -r :ACTIVE: -e 0,0,0,-1,-1
else
  #set focus
  wmctrl -a Janit_MENU
  wmctrl -r :ACTIVE: -e 0,0,0,-1,-1
fi



#PS1="\[\e]0;Janit\a\]\u@\h:\w\$ "
