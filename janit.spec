#Janit GPL3
#Copyright (C) 2018 David Hamner

#This program is free software: you can redistribute it and/or modify
#it under the terms of the GNU General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.

#This program is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU General Public License for more details.

#You should have received a copy of the GNU General Public License
#along with this program. If not, see <http://www.gnu.org/licenses/>.


# spec file for package myjanit

Name:           janit
Version:        1
Release:        18
Summary:        Terminal multiplexer and menu interface


Group:          TecAdmin
BuildArch:      noarch
License:        GPL
URL:            http://chimeraos.com/
Source0:        janit-1.0.tar.gz
Requires:       python3
Requires:       python3-setuptools
Requires:       python3-pyte
Requires:       python3-curses
Requires:       python3-ewmh
Recommends:     xautomation

%description
Write some description about your package here

%prep
%setup -q
%build
%install
install -m 0755 -d $RPM_BUILD_ROOT/usr/share/janit
install -m 0755 -d $RPM_BUILD_ROOT/usr/share/janit/Janit
install -m 0755 -d $RPM_BUILD_ROOT/usr/share/janit/Janit/core
install -m 0755 -d $RPM_BUILD_ROOT/usr/share/janit/Janit/cmd
install -m 0755 -d $RPM_BUILD_ROOT/usr/share/janit/bin
install -m 0755 -d $RPM_BUILD_ROOT/usr/share/janit/Examples
install -m 0755 -d $RPM_BUILD_ROOT/usr/share/janit/Examples/Configs
install -m 0755 -d $RPM_BUILD_ROOT/usr/share/janit/Examples/Configs/serial

install -m 0755 janit.py $RPM_BUILD_ROOT/usr/share/janit/janit.py
install -m 0644 README.md $RPM_BUILD_ROOT/usr/share/janit/README.md

#only need r/w for root and just read for others
install -m 0644 Janit/__init__.py $RPM_BUILD_ROOT/usr/share/janit/Janit/__init__.py

#core things
install -m 0644 Janit/core/bash.py $RPM_BUILD_ROOT/usr/share/janit/Janit/core/bash.py
install -m 0644 Janit/core/chroot.py $RPM_BUILD_ROOT/usr/share/janit/Janit/core/chroot.py
install -m 0644 Janit/core/core.py $RPM_BUILD_ROOT/usr/share/janit/Janit/core/core.py
install -m 0644 Janit/core/desktop.py $RPM_BUILD_ROOT/usr/share/janit/Janit/core/desktop.py
install -m 0644 Janit/core/display.py $RPM_BUILD_ROOT/usr/share/janit/Janit/core/display.py
install -m 0644 Janit/core/menu.py $RPM_BUILD_ROOT/usr/share/janit/Janit/core/menu.py
install -m 0644 Janit/core/mouse.py $RPM_BUILD_ROOT/usr/share/janit/Janit/core/mouse.py
install -m 0644 Janit/core/ssh.py $RPM_BUILD_ROOT/usr/share/janit/Janit/core/ssh.py

#commands
install -m 0644 Janit/cmd/example.py $RPM_BUILD_ROOT/usr/share/janit/Janit/cmd/example.py
install -m 0644 Janit/cmd/fun.py $RPM_BUILD_ROOT/usr/share/janit/Janit/cmd/fun.py
install -m 0644 Janit/cmd/keyboard.py $RPM_BUILD_ROOT/usr/share/janit/Janit/cmd/keyboard.py
install -m 0644 Janit/cmd/liveusb.py $RPM_BUILD_ROOT/usr/share/janit/Janit/cmd/liveusb.py
install -m 0644 Janit/cmd/man.py $RPM_BUILD_ROOT/usr/share/janit/Janit/cmd/man.py
install -m 0644 Janit/cmd/nspawn.py $RPM_BUILD_ROOT/usr/share/janit/Janit/cmd/nspawn.py
install -m 0644 Janit/cmd/serial.py $RPM_BUILD_ROOT/usr/share/janit/Janit/cmd/serial.py
install -m 0644 Janit/cmd/shell.py $RPM_BUILD_ROOT/usr/share/janit/Janit/cmd/shell.py
install -m 0644 Janit/cmd/ssh.py $RPM_BUILD_ROOT/usr/share/janit/Janit/cmd/shh.py


install -m 0644 bin/openJanit.sh $RPM_BUILD_ROOT/usr/share/janit/bin/openJanit.sh
install -m 0644 bin/setupChroots.sh $RPM_BUILD_ROOT/usr/share/janit/bin/setupChroots.sh
install -m 0644 bin/testCli.py $RPM_BUILD_ROOT/usr/share/janit/bin/testCli.py


install -m 0644 Examples/Configs/serial/bashrc.txt $RPM_BUILD_ROOT/usr/share/janit/Examples/Configs/serial/bashrc.txt
install -m 0644 Examples/Configs/serial/janit.cfg $RPM_BUILD_ROOT/usr/share/janit/Examples/Configs/serial/janit.cfg


#install -m 0644 README.md $RPM_BUILD_ROOT/etc/mydumpadmin/README.md
#install -m 0644 settings.conf $RPM_BUILD_ROOT/etc/mydumpadmin/settings.conf

%post
ln -s $RPM_BUILD_ROOT/usr/share/janit/janit.py $RPM_BUILD_ROOT/usr/bin/janit || true

%files
/usr/share/janit
/usr/share/janit/janit.py
#/etc/mydumpadmin/mysql-dump.sh
#/etc/mydumpadmin/README.md
#/etc/mydumpadmin/settings.conf

%changelog
* Sat Mar 21 2019 David Hamner  0.1
  - Initial rpm release
